package org.openinsurer.validationapi;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.openinsurer.data.SsnInfo;
import org.openinsurer.data.DriverLicenseInfo;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class ValidationController {

    @RequestMapping("/")
    public String index() {
        return "Greetings from Validation Controller!";
    }

/*
    @RequestMapping("/validationapi/")
    public String index() {
        return "Greetings from Validation Controller!";
    }
*/
    
    @RequestMapping("/validationapi/validateSsn")
    public SsnInfo validateSsn(@RequestParam(value="ssn", required=false) String ssn) {
    	SsnInfo ssnInfo = new SsnInfo();
    	ssnInfo.setSsn(ssn);
    	ssnInfo.setValid(ssn.length()== 9);    	//valid driver license number should have length = 9
    	System.out.println(ssnInfo.getSsn());       
    	System.out.println(ssnInfo.isValid());
    	return ssnInfo;
    }
    
    @RequestMapping("/validationapi/validateDriverLicense")
    public DriverLicenseInfo validateDriverLicense(@RequestParam(value="state", required=true) String state, 
    									@RequestParam(value="dl", required=true) String driverLicense) {
    	DriverLicenseInfo dlInfo = new DriverLicenseInfo();
    	dlInfo.setState(state);
    	dlInfo.setLicenseNumber(driverLicense);
    	dlInfo.setValid(driverLicense.length()>7); //valid driver license number should have length > 7
        return dlInfo;        
    }

   
}