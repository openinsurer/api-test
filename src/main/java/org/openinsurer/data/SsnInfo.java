package org.openinsurer.data;

public class SsnInfo {
	
	private String ssn = null;
	private boolean isValid = false;

	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
}
